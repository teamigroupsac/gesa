var service = new Service("webService/index.php");


$(function() 
{

iniciarControlGenerarArchivos();

});

var arrayTiendas = [];

function iniciarControlGenerarArchivos(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioTienda').val(fecha);

    



    $("#menu_generar_archivos").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_generar_archivos").on('click', function(){
      cargaConsultasIniciales();  
    })
    
    function cargaConsultasIniciales(){
        var file = "usuarios";
        service.procesar("comprobarInformacionArchivoG",file,resultadoGenerarArchivoUsuario);
        var file = "maestro";
        service.procesar("comprobarInformacionArchivoG",file,resultadoGenerarArchivoMaestro);
        var file = "maestro_detalle";
        service.procesar("comprobarInformacionArchivoG",file,resultadoGenerarArchivoMaestroDetalle);
        var file = "BARRA";
        service.procesar("comprobarInformacionArchivoG",file,resultadoGenerarArchivoTottus1);
        var file = "AREA_BARRA";
        service.procesar("comprobarInformacionArchivoG",file,resultadoGenerarArchivoTottus2);
        var file = "archivo_diferencias";
        service.procesar("comprobarInformacionArchivoG",file,resultadoGenerarArchivoReporteDiferencias);
    }


    $("#generar_archivos .usuarios .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO USUARIO ?", function (e) {
            if (e) {
                openModalGenerarArchivo("USUARIO");
                service.procesar("generarArchivoUsuario",1,function(evt){
                    resultadoGenerarArchivoUsuario(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoUsuario(evt){
        var resultado = evt;
        $("#generar_archivos .usuarios .peso").html(resultado[0].peso);
        $("#generar_archivos .usuarios .filas").html(resultado[0].filas);
        $("#generar_archivos .usuarios .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .usuarios .descargararchivo").on('click', function(){
        var file = 'usuarios';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .maestro .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO MAESTRO ?", function (e) {
            if (e) {
                openModalGenerarArchivo("MAESTRO");
                service.procesar("generarArchivoMaestro",function(evt){
                    resultadoGenerarArchivoMaestro(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoMaestro(evt){
        var resultado = evt;
        $("#generar_archivos .maestro .peso").html(resultado[0].peso);
        $("#generar_archivos .maestro .filas").html(resultado[0].filas);
        $("#generar_archivos .maestro .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .maestro .descargararchivo").on('click', function(){
        var file = 'maestro';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .maestro-detalle .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO MAESTRO DETALLE ?", function (e) {
            if (e) {
                openModalGenerarArchivo("MAESTRO DETALLE");
                service.procesar("generarArchivoMaestroDetalle",function(evt){
                    resultadoGenerarArchivoMaestroDetalle(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoMaestroDetalle(evt){
        var resultado = evt;
        $("#generar_archivos .maestro-detalle .peso").html(resultado[0].peso);
        $("#generar_archivos .maestro-detalle .filas").html(resultado[0].filas);
        $("#generar_archivos .maestro-detalle .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .maestro-detalle .descargararchivo").on('click', function(){
        var file = 'maestro_detalle';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })



    function openModalGenerarArchivo(archivo){
        $("#modal_generar_archivos_progress").modal('show');
        $("#modal_generar_archivos_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_generar_archivos_progress").modal('hide');
        alertify.success("ARCHIVO ACTUALIZADO SATISFACTORIAMENTE");
    }




    $("#generar_archivos .archivo-tottus-1 .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO BARRA ?", function (e) {
            if (e) {
                openModalGenerarArchivo("PRIMER ARCHIVO");
                service.procesar("generarprimerarchivo",function(evt){
                    resultadoGenerarArchivoTottus1(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoTottus1(evt){
        var resultado = evt;
        $("#generar_archivos .archivo-tottus-1 .peso").html(resultado[0].peso);
        $("#generar_archivos .archivo-tottus-1 .filas").html(resultado[0].filas);
        $("#generar_archivos .archivo-tottus-1 .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .archivo-tottus-1 .descargararchivo").on('click', function(){
        var file = 'BARRA';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .archivo-tottus-2 .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO AREA BARRA ?", function (e) {
            if (e) {
                openModalGenerarArchivo("SEGUNDO ARCHIVO");
                service.procesar("generarsegundoarchivo",function(evt){
                    resultadoGenerarArchivoTottus2(evt);
                    closeModalGenerarArchivo();
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoTottus2(evt){
        var resultado = evt;
        $("#generar_archivos .archivo-tottus-2 .peso").html(resultado[0].peso);
        $("#generar_archivos .archivo-tottus-2 .filas").html(resultado[0].filas);
        $("#generar_archivos .archivo-tottus-2 .fecha").html(resultado[0].fecha);
    }

    $("#generar_archivos .archivo-tottus-2 .descargararchivo").on('click', function(){
        var file = 'AREA_BARRA';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })


    $("#generar_archivos .archivo-diferencias .generararchivo").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO DE REPORTE DE DIFERENCIAS ?", function (e) {
            if (e) {
                var estado = 1;
                service.procesar("generarReporteDiferencias",estado,resultadoGenerarArchivoReporteDiferencias);
                openModalGenerarArchivo("REPORTE DE DIFERENCIAS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoReporteDiferencias(evt){
        var resultado = evt;
        $("#generar_archivos .archivo-diferencias .peso").html(resultado[0].peso);
        $("#generar_archivos .archivo-diferencias .filas").html(resultado[0].filas);
        $("#generar_archivos .archivo-diferencias .fecha").html(resultado[0].fecha);
        closeModalGenerarArchivo();
    }

    $("#generar_archivos .archivo-diferencias .descargararchivo").on('click', function(){
        var file = 'archivo_diferencias';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })

    $("#generar_archivos .archivo-diferencias .excelarchivo").on('click', function(){
        service.procesar("getDescargaReporteDiferencias",function(evt){
            document.location = "webService/reportes/reportediferencias.xlsx"
        });
    })




    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

}


