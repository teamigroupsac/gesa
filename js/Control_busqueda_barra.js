var service = new Service("webService/index.php");


$(function() 
{

iniciarControlBusquedaBarra();

});

var arrayCapturas = [];

function iniciarControlBusquedaBarra(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    //$('#fechaFormularioUsuario').val(fecha);

    




    $("#menu_busqueda_barra").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_busqueda_barra").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){

        $(".opcionesBusquedaBarra").css("display","none");

    }

    
    /*function cargaListaAreaCap(evt){
        resultado = evt;
        $("#areaCapFormularioCaptura").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#areaCapFormularioCaptura" ).append( "<option value='"+ resultado[i].area_cap +"'>"+ resultado[i].area_cap +"</option>" );
        }
    }*/

    function aplicarFiltroBusquedaBarra(){
        var barra_captura = $("#barraCapFormularioBusquedaBarra").val();
        service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);

    }

    $('#botonBuscarFormularioBusquedaBarra').on('click',function(){
        if($('#barraCapFormularioBusquedaBarra').val() != ""){
            aplicarFiltroBusquedaBarra();    
        }else{
            alertify.error("INGRESE VALOR A CAMPO BARRA CAP");
        }
        
    });





    function cargaListaBusquedaBarra(evt){
        
        listadeBarras = evt.registros;

        $("#tablaResultadoFormularioBusquedaBarra thead").html("<tr><th>#</th><th>AREA_CAP</th><th>BARRA_CAP</th><th>DES_BARRA</th><th>CANT_CAP</th><th>TIP_CAP</th><th>USUARIO</th><th>DESCARGADO</th><th>NAMEPALM</th><th>MARCAR</th><th>OPCIONES</th></tr>");
        $("#tablaResultadoFormularioBusquedaBarra tbody").html("");
        $(".areaBusquedaAreaCapFormularioBusquedaBarra").css("display","none");

        //LLENADO DE INFORMACION EN EL AREA DE AVANCE
        //service.procesar("getListaAreaRango",resultadoAvanceCapturaInicial);

        if ( listadeBarras == undefined ) return;

        $("#tablaResultadoFormularioBusquedaBarra thead").html("<tr><th># ( "+listadeBarras.length+" )</th><th>AREA_CAP</th><th>BARRA_CAP ( "+evt.barras[0].cont_barra+" )</th><th>DES_BARRA</th><th>CANT_CAP ( "+parseFloat(evt.cantidad[0].sum_cant)+" )</th><th>TIP_CAP</th><th>USUARIO</th><th>DESCARGADO</th><th>NAMEPALM</th><th>MARCAR</th><th>OPCIONES</th></tr>");
        $(".areaBusquedaAreaCapFormularioBusquedaBarra").css("display","");
        $("#registrosFormularioBusquedaBarra").html();


        for(var i=0; i<listadeBarras.length ; i++){

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = listadeBarras[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeBarras[i].area_cap +'</td><td>'+ listadeBarras[i].barra_cap +'</td><td>'+ listadeBarras[i].des_barra +'</td><td>'+ listadeBarras[i].cant_cap +'</td><td>'+ listadeBarras[i].tip_cap +'</td><td>'+ listadeBarras[i].nombreUsuario +'</td><td>'+ listadeBarras[i].descargado +'</td><td>'+ listadeBarras[i].namepalm +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioBusquedaBarra);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            var contenedorBotones = $("<td>");

            

            var btnSeleccionar = $("<button class='btn btn-primary btn-sm botonesCaptura' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarFormularioBusquedaBarra);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesCaptura' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioBusquedaBarra);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaResultadoFormularioBusquedaBarra tbody").append(fila);

        }

    }

    function seleccionarFormularioBusquedaBarra(){
        var data = $(this).data("data");
        var id_captura = data.id_captura;
        var valor = $.inArray(id_captura, arrayCapturas);

        if(valor >= 0){
        	removeItemFromArr( arrayCapturas, id_captura );
        }else{
        	arrayCapturas.push(id_captura);
        }
        
        var contar = arrayCapturas.length;

        if(contar > 1){
        	$("#botonCancelarBusquedaBarra").css("display","");
            //$("#botonGuardarCapturas").html("MODIFICAR SELECCIONADOS");
            $("#botonCancelarBusquedaBarra").html("ELIMINAR SELECCIONADOS");

            $(".botonesBusquedaBarra").prop( "disabled", true );

            $("#codigoFormularioBusquedaBarra").val("");
            //$("#barraCapFormularioBusquedaBarra").val("");
            $("#cantCapFormularioBusquedaBarra").val("");
        }else{
        	$(".opcionesBusquedaBarra").css("display","none");
            $("#botonGuardarBusquedaBarra").html("MODIFICAR");
            $("#botonCancelarBusquedaBarra").html("CANCELAR");

            $(".botonesCaptura").prop( "disabled", false );
        }
    }

    function editarFormularioBusquedaBarra(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL REGISTRO : " + data.id_captura +" ?", function (e) {
            if (e) {
                $("#codigoFormularioBusquedaBarra").val(data.id_captura);
                //$("#barraCapFormularioBusquedaBarra").val(data.barra_cap);
                $("#cantCapFormularioBusquedaBarra").val(data.cant_cap);

                $("#botonGuardarBusquedaBarra").html("MODIFICAR");
                $("#botonCancelarBusquedaBarra").html("CANCELAR");

                $(".opcionesBusquedaBarra").css("display","");


                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarFormularioBusquedaBarra(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL REGISTRO : " + data.id_captura +" ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioBusquedaBarra").val();
                var objetoFormulario = new Object()
                    objetoFormulario.id_captura = data.id_captura;
                    objetoFormulario.responsable = responsable;

                service.procesar("deleteFormularioBusquedaBarra",objetoFormulario,resultadoDeleteFormularioBusquedaBarra);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioBusquedaBarra(evt){
        var barra_captura = $("#barraCapFormularioBusquedaBarra").val();
        service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);
    }

    $("#botonGuardarBusquedaBarra").on('click', function(){ //PENDIENTES

        var procedimiento = $("#botonGuardarBusquedaBarra").html(); //TITULO DEL BOTON

        if(procedimiento == "MODIFICAR"){

            var id_captura = $("#codigoFormularioBusquedaBarra").val();
            var responsable = $("select#responsableFormularioBusquedaBarra").val();
            var cant_cap  = $("#cantCapFormularioBusquedaBarra").val();

            if(id_captura != "" && cant_cap != ""){

                alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                    if (e) {

                        var objetoFormulario = new Object()
                            objetoFormulario.procedimiento = procedimiento;
                            objetoFormulario.id_captura = id_captura;
                            objetoFormulario.responsable = responsable;
                            objetoFormulario.cant_cap = cant_cap;
                            objetoFormulario.usuario = usuario;

                        service.procesar("saveFormularioBusquedaBarra",objetoFormulario,cargaFormularioBusquedaBarra);
                        alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                    } else {
                        alertify.error("HA CANCELADO EL PROCESO");
                    }
                });

            }else{
                alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
            }

        }else{

            //guardarMasivoFormularioBusquedaBarra();

        }





    })

    $("#botonCancelarBusquedaBarra").on('click',function(){
        var procedimiento = $("#botonCancelarBusquedaBarra").html();

        if(procedimiento == "CANCELAR"){
            alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
                if (e) {
    		        alertify.success("PROCESO CANCELADO");
                    $(".opcionesBusquedaBarra").css("display","none");
    		        formularioBusquedaBarraEstandar();
                }
            });
        }else{
            eliminarMasivoFormularioBusquedaBarra();
        }

    })

    //$("#botonGuardarEstadoMasivoFormularioCaptura").on('click',function(){
    /*function guardarMasivoFormularioBusquedaBarra(){
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS MASIVOS ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioBusquedaBarra").val();
                var objetoFormulario = new Object()
                    objetoFormulario.estado = $("select#estadoMasivoFormularioBusquedaBarra").val();
                    objetoFormulario.responsable = responsable;
                    objetoFormulario.cant_cap = $("#cantCapFormularioBusquedaBarra").val();
                    objetoFormulario.capturas = arrayCapturas;
                    objetoFormulario.usuario = usuario;

                service.procesar("modificarMasivoFormularioBusquedaBarra",objetoFormulario,cargaModificarMasivoFormularioBusquedaBarra);

                alertify.success("HA ACEPTADO MODIFICAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }*/

    //$("#botonEliminarMasivoFormularioCaptura").on('click',function(){
    function eliminarMasivoFormularioBusquedaBarra(){
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS MASIVOS ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioBusquedaBarra").val();
                var objetoFormulario = new Object()
                    objetoFormulario.capturas = arrayCapturas;
                    objetoFormulario.responsable = responsable;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarMasivoFormularioBusquedaBarra",objetoFormulario,cargaEliminarMasivoFormularioBusquedaBarra);

                alertify.success("HA ACEPTADO ELIMINAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function cargaEliminarMasivoFormularioBusquedaBarra(evt){
    	if(evt == 1 ){
            var barra_captura = $("#barraCapFormularioBusquedaBarra").val();
            alertify.success("REGISTROS ELIMINADOS SATISFACTORIAMENTE");
            //$(".opciones").css("display","");
        	//$(".opcionesmasivo").css("display","none");
        	arrayCapturas.length=0;
            $(".opcionesBusquedaBarra").css("display","none");
            service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    /*    if(contar > 1){
            $(".opcionesCaptura").css("display","none");
            $(".botonesCaptura").prop( "disabled", true );
        }else{
            $(".opcionesCaptura").css("display","");
            $(".botonesCaptura").prop( "disabled", false );
        }
    */
   
    function cargaModificarMasivoFormularioBusquedaBarra(evt){
    	if(evt == 1 ){
            var barra_captura = $("#barraCapFormularioBusquedaBarra").val();  
            alertify.success("REGISTROS GUARDADOS SATISFACTORIAMENTE");


            //$(".opcionesCaptura").css("display","");
            //$(".botonesCaptura").prop( "disabled", true );

            $("#codigoFormularioBusquedaBarra").val("");
            $("#barraCapFormularioBusquedaBarra").val("");
            $("#cantCapFormularioBusquedaBarra").val("");

        	arrayCapturas.length=0;
            $(".opcionesBusquedaBarra").css("display","none");
            service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaFormularioBusquedaBarra(evt){
        var barra_captura = $("#barraCapFormularioBusquedaBarra").val();        
        if(evt == 1 ){
            alertify.success("REGISTRO GUARDADO SATISFACTORIAMENTE");
            service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);
            formularioBusquedaBarraEstandar(barra_captura);
        }else if(evt == 2 ){
            alertify.success("REGISTRO MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);
            $(".opcionesBusquedaBarra").css("display","none");
            formularioBusquedaBarraEstandar(barra_captura);
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }
    }

    function formularioBusquedaBarraEstandar(barra_captura){
        $("#barraCapFormularioBusquedaBarra").val(barra_captura);
        $("#cantCapFormularioBusquedaBarra").val("");
        $("#codigoFormularioBusquedaBarra").val("");
        $("select#areaCapFormularioBusquedaBarra").val(0);
        $("#botonGuardarBusquedaBarra").html("GUARDAR");
    }


    $("#botonEliminarBarraFormularioBusquedaBarra").on('click',function(){

        alertify.confirm("¿ SEGURO DE ELIMINAR BARRA ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioBusquedaBarra").val();
                var barra_cap = $("#barraCapFormularioBusquedaBarra").val();
                var objetoFormulario = new Object()
                    objetoFormulario.barra = barra_cap;
                    objetoFormulario.responsable = responsable;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarBarraCapFormularioBusquedaBarra",objetoFormulario,resultadoEACFBB);

                alertify.success("HA ACEPTADO ELIMINAR BARRA CAP");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });

    })


    function resultadoEACFBB(evt){
        var barra_captura = $("#barraCapFormularioBusquedaBarra").val();        
        if(evt == 1 ){
            alertify.success("REGISTRO ELIMINADO SATISFACTORIAMENTE");
            service.procesar("getListaBusquedaBarra",barra_captura,cargaListaBusquedaBarra);
            formularioBusquedaBarraEstandar(barra_captura);
        }else{
            alertify.error("REGISTRO NO ELIMINAD"); 
        }

    }





    //LLENADO DE INFORMACION EN EL AREA DE AVANCE

    function resultadoAvanceCapturaInicial(evt){
        listadeAreaRango = corregirNullArreglo(evt.rangos);
        listaCapturas = corregirNullArreglo(evt.capturas);
        listaJustificados = corregirNullArreglo(evt.justificados);

        var cantidad = 0;
        var avance = 0;
        var porcentaje = 0;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);

            for (var z=filaInicio; z<=filaFinal; z++) {
                cantidad++;
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                    }
                }
            }

        }

        porcentaje = ((parseFloat(avance) / parseFloat(cantidad))*100).toFixed(2);

        $(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
        $(".progressAvanceCaptura").html(porcentaje+' %');      
    }


    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }



}


