<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceCambiaLote extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaCambiaLote($dato){
		$area_cap = $dato;
		$condicion = "";
		if ($area_cap > 0){
			$condicion = "WHERE area_cap = '$area_cap'";
		}
		$sql = "SELECT A.*, B.nombreUsuario, C.des_barra FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
                ORDER BY A.id_captura ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

		$sqlCantidad = "SELECT SUM(cant_cap) sum_cant FROM captura $condicion";
		$resCantidad = $this->db->get_results($sqlCantidad);

		$registros = new stdClass();
        $registros->registros = $res;
        $registros->cantidad = $resCantidad;

		return $registros;
	}

	function modificaLoteFormularioCambiaLote($data){
		$valorActual = $data->valorActual;
		$valorNuevo = $data->valorNuevo;

		$sql="UPDATE captura SET area_cap = '$valorNuevo' WHERE area_cap = '$valorActual'";
        $resEditar=$this->db->query($sql);

        if($resEditar){
            return 1;
        }else{
            return 0;
        }

	}








}	
?>