
<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceGenerarArchivos extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function generarArchivoUsuario($dato){
		$condicion = "";
		if($dato != ""){
			$condicion = "WHERE estadoUsuario = $dato";
		}

		$sql = "SELECT dniUsuario,claveUsuario,tipoUsuario FROM usuario $condicion";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/usuarios.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->dniUsuario."|".$res[$i]->claveUsuario."|".$res[$i]->tipoUsuario;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}

	function generarArchivoMaestro(){

		$sql = "SELECT DISTINCT cod_barra FROM maestro";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}

	function generarArchivoMaestroDetalle(){

		$sql = "SELECT cod_barra, des_barra FROM maestro GROUP BY cod_barra";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro_detalle.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra."|".$res[$i]->des_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}


	function comprobarInformacionArchivoG($file){
		$archivo = "../archivos_sistema/archivos_generados/".$file.".txt";
		$bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
    	$conteo = count(file($archivo));

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = ($conteo - 1);
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}


	function generarprimerarchivo(){
		$sql="	SELECT A.barra_cap, B.des_barra, SUM(A.cant_cap) cant_cap FROM captura A LEFT JOIN maestro B
				ON A.barra_cap = B.cod_barra
				GROUP BY A.barra_cap
				ORDER BY SUM(A.cant_cap) ASC ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/BARRA.txt";
		unlink('$archivo');

		$cadena="\r\n";
		$conteo=1;

		$cadena.="BARRA|DES_BARRA|CANT_CAP";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->barra_cap."|".$res[$i]->des_barra."|".$res[$i]->cant_cap;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarsegundoarchivo(){
		$sql="	SELECT A.area_cap, A.barra_cap, B.des_barra, SUM(A.cant_cap) cant_cap FROM captura A LEFT JOIN maestro B
				ON A.barra_cap = B.cod_barra
				GROUP BY A.area_cap, A.barra_cap
				ORDER BY SUM(A.cant_cap) ASC ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/AREA_BARRA.txt";
		unlink('$archivo');

		$cadena="\r\n";
		$conteo=1;

		$cadena.="AREA|BARRA|DES_BARRA|CANT_CAP";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->area_cap."|".$res[$i]->barra_cap."|".$res[$i]->des_barra."|".$res[$i]->cant_cap;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarReporteDiferencias(){
		$sql="SELECT * FROM diferencias";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/archivo_diferencias.txt";
		unlink('$archivo');

		$conteo=1;
		$cadena.="\r\n";
		$cadena.="SKU|DES_SKU|COSTO|STOCK|CAPTURA|DIF_CANT|STOCK_SOL|CAPTURA_SOL|DIF_SOL|DIF_SOL_POS|DEP_STK|BARRA_CAP";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $conteo."|".$res[$i]->sku_stk."|".$res[$i]->des_sku_stk."|".$res[$i]->costo."|".$res[$i]->stock."|".$res[$i]->contado."|".$res[$i]->dif_cant."|".$res[$i]->total_stock_sol."|".$res[$i]->total_cap_sol."|".$res[$i]->dif_sol."|".$res[$i]->dif_sol_pos."|".$res[$i]->dep_stk."|".$res[$i]->barra_cap;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function getDescargaReporteDiferencias(){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

		$sql="SELECT * FROM diferencias";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("sku_stk","des_sku_stk","dep_stk"));

		if($res){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel");

			$tituloReporte = "REPORTE DE DIFERENCIAS";
			$titulosColumnas = array('N','SKU','DES_SKU','COSTO','STOCK','CAPTURA','DIF_CANT','STOCK_SOL','CAPTURA_SOL','DIF_SOL','DIF_SOL_POS','DEP_STK','BARRA_CAP');
			
			$objPHPExcel->setActiveSheetIndex(0)
	        		    ->mergeCells('A1:H1');
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1',$tituloReporte)
						->setCellValue('L1','FECHA : ')
						->setCellValue('M1',$fecha)
						->setCellValue('L2','HORA : ')
						->setCellValue('M2',$hora)
	        		    ->setCellValue('A3',  $titulosColumnas[0])
			            ->setCellValue('B3',  $titulosColumnas[1])
	        		    ->setCellValue('C3',  $titulosColumnas[2])
	            		->setCellValue('D3',  $titulosColumnas[3])
	            		->setCellValue('E3',  $titulosColumnas[4])
	            		->setCellValue('F3',  $titulosColumnas[5])
	            		->setCellValue('G3',  $titulosColumnas[6])
	            		->setCellValue('H3',  $titulosColumnas[7])
	            		->setCellValue('I3',  $titulosColumnas[8])
	            		->setCellValue('J3',  $titulosColumnas[9])
	            		->setCellValue('K3',  $titulosColumnas[10])
	            		->setCellValue('L3',  $titulosColumnas[11])
	            		->setCellValue('M3',  $titulosColumnas[12]);
			
			//Se agregan los datos de los alumnos
			$i = 4;

			for( $x = 0; $x < count($res); $x++)
			{

				$objPHPExcel->setActiveSheetIndex(0)
	        		    ->setCellValue('A'.$i,  ($x + 1))
			            ->setCellValue('B'.$i,  $res[$x]->sku_stk)
	        		    ->setCellValue('C'.$i,  $res[$x]->des_sku_stk)
	            		->setCellValue('D'.$i,  $res[$x]->costo)
	            		->setCellValue('E'.$i,  $res[$x]->stock)
	            		->setCellValue('F'.$i,  $res[$x]->contado)
	            		->setCellValue('G'.$i,  $res[$x]->dif_cant)
	            		->setCellValue('H'.$i,  $res[$x]->total_stock_sol)
	            		->setCellValue('I'.$i,  $res[$x]->total_cap_sol)
	            		->setCellValue('J'.$i,  $res[$x]->dif_sol)
	            		->setCellValue('K'.$i,  $res[$x]->dif_sol_pos)
	            		->setCellValue('L'.$i,  $res[$x]->dep_stk)
	            		->setCellValue('M'.$i,  $res[$x]->barra_cap);
						$i++;
			}

			
			$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));
			 
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			for($i = 'A'; $i <= 'H'; $i++){
				$objPHPExcel->setActiveSheetIndex(0)			
					->getColumnDimension($i)->setAutoSize(TRUE);
			}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reportediferencias.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reportediferencias.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}



}	
?>