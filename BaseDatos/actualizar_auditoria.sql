-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para inventariostt
CREATE DATABASE IF NOT EXISTS `inventariostt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `inventariostt`;

-- Volcando estructura para tabla inventariostt.auditoria
CREATE TABLE IF NOT EXISTS `auditoria` (
  `idAuditoria` int(11) NOT NULL AUTO_INCREMENT,
  `area_cap` varchar(5) NOT NULL DEFAULT '0',
  `barra_cap` varchar(15) NOT NULL,
  `cant_cap_ant` decimal(17,3) NOT NULL DEFAULT '0.000',
  `cant_cap_act` decimal(17,3) NOT NULL DEFAULT '0.000',
  `tip_cap` int(1) NOT NULL DEFAULT '0',
  `tipo` varchar(50) NOT NULL DEFAULT '0',
  `responsable` varchar(50) NOT NULL DEFAULT '0',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idAuditoria`),
  KEY `barra_cap` (`barra_cap`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla inventariostt.auditoria: 1 rows
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` (`idAuditoria`, `area_cap`, `barra_cap`, `cant_cap_ant`, `cant_cap_act`, `tip_cap`, `tipo`, `responsable`, `fecha`) VALUES
	(1, '6576', '2000869750007', 5.000, 0.000, 2, 'ELIMINADO', 'CLIENTE', '2018-06-26 15:59:17');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
