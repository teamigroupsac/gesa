-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para inventariostt
CREATE DATABASE IF NOT EXISTS `inventariostt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `inventariostt`;

-- Volcando estructura para tabla inventariostt.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `idPerfil` int(10) NOT NULL AUTO_INCREMENT,
  `enlace` longtext NOT NULL,
  `titulo` longtext NOT NULL,
  `ventanas` longtext NOT NULL,
  `control` varchar(50) NOT NULL,
  `permiso` int(11) NOT NULL,
  PRIMARY KEY (`idPerfil`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla inventariostt.permisos: 13 rows
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`idPerfil`, `enlace`, `titulo`, `ventanas`, `control`, `permiso`) VALUES
	(1, 'usuarios', 'USUARIOS', 'usuarios', 'usuarios', 1),
	(2, 'capturas', 'CAPTURAS', 'capturas', 'capturas', 4),
	(4, 'archivos_pendientes', 'ARCHIVOS PENDIENTES', 'archivos_pendientes', 'archivos_pendientes', 10),
	(3, 'maestro', 'CARGA MAESTRO', 'maestro', 'maestro', 7),
	(6, 'stock', 'CARGA STOCK', 'stock', 'stock', 9),
	(7, 'area_rango', 'AREA RANGO', 'area_rango', 'area_rango', 11),
	(8, 'tienda', 'TIENDA', 'tienda', 'tienda', 3),
	(9, 'tipo_usuario', 'TIPO USUARIO', 'tipo_usuario', 'tipo_usuario', 2),
	(10, 'busqueda_barra', 'BUSQUEDA BARRA', 'busqueda_barra', 'busqueda_barra', 6),
	(11, 'cambia_lote', 'CAMBIA LOTE', 'cambia_lote', 'cambia_lote', 8),
	(12, 'generar_archivos', 'GENERAR ARCHIVOS', 'generar_archivos', 'generar_archivos', 12),
	(13, 'generar_cierre', 'GENERAR CIERRE', 'generar_cierre', 'generar_cierre', 13),
	(14, 'reporte_auditoria', 'REPORTE DE AUDITORIA', 'reporte_auditoria', 'reporte_auditoria', 5);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `dniUsuario` varchar(8) CHARACTER SET latin1 NOT NULL,
  `tipoUsuario` varchar(50) NOT NULL,
  `claveUsuario` varchar(50) CHARACTER SET latin1 NOT NULL,
  `nombreUsuario` varchar(250) NOT NULL,
  `estadoUsuario` int(1) NOT NULL,
  `fechaUsuario` date NOT NULL,
  `permisosUsuario` varchar(50) NOT NULL,
  `usuarioRegistrador` varchar(50) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `usuario` (`dniUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Table ''bd_acreditacion.usuario_1'' doesn''t exist in engine';

-- Volcando datos para la tabla inventariostt.usuario: 6 rows
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`idUsuario`, `dniUsuario`, `tipoUsuario`, `claveUsuario`, `nombreUsuario`, `estadoUsuario`, `fechaUsuario`, `permisosUsuario`, `usuarioRegistrador`) VALUES
	(1, '41467062', '1', '210382', 'SALCEDO PEÑA, CARLOS SAUL', 1, '2018-03-22', '1,2,3,4,5,6,7,8,9,10,11,12,13', '41467062'),
	(2, '42845381', '1', '123456', 'OSCAR CHAVARRI CABALLERO', 1, '2018-02-26', '1,2,3,4,5,6,7,8,9,10,11,12,13', '41467062'),
	(4, '1234', '2', '123', 'USUARIO NUEVO', 1, '2018-03-21', '', '41467062'),
	(5, '1234', '2', '1234', 'FRANCO VALERA', 1, '2018-02-27', '', '41467062'),
	(6, '2323', '2', '1234', 'REYNA ROJAS', 1, '2018-02-27', '', '41467062'),
	(7, '32323', '2', 'DDDD', 'JUAN ALBERTO CASAS', 1, '2018-03-22', '', '41467062');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
