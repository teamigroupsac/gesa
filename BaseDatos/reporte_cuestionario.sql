-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para inventariostt
CREATE DATABASE IF NOT EXISTS `inventariostt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `inventariostt`;

-- Volcando estructura para tabla inventariostt.reporte
CREATE TABLE IF NOT EXISTS `reporte` (
  `idReporte` int(11) NOT NULL AUTO_INCREMENT,
  `reporte` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idReporte`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reporte: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `reporte` DISABLE KEYS */;
INSERT INTO `reporte` (`idReporte`, `reporte`) VALUES
	(1, 'INVENTARIO - ALMACEN/BODEGA'),
	(2, 'INVENTARIO - PISO DE VENTA');
/*!40000 ALTER TABLE `reporte` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportecuestionario
CREATE TABLE IF NOT EXISTS `reportecuestionario` (
  `idCuestionario` int(11) NOT NULL AUTO_INCREMENT,
  `cuestionario` varchar(255) DEFAULT NULL,
  `idReporte` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCuestionario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportecuestionario: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `reportecuestionario` DISABLE KEYS */;
INSERT INTO `reportecuestionario` (`idCuestionario`, `cuestionario`, `idReporte`) VALUES
	(1, '1. PREPARACION DE INVENTARIO', 1),
	(2, '2. PROCESO DE CONTEO - INVENTARIO', 1),
	(3, '1. LOTIZACION - PISO VENTA', 2),
	(4, '2. CAPTURA - PISO VENTA', 2);
/*!40000 ALTER TABLE `reportecuestionario` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportegrupo
CREATE TABLE IF NOT EXISTS `reportegrupo` (
  `idGrupo` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idGrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportegrupo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `reportegrupo` DISABLE KEYS */;
INSERT INTO `reportegrupo` (`idGrupo`, `grupo`) VALUES
	(1, 'DICOTOMICA'),
	(2, 'INTENSION'),
	(3, 'SATISFACCION');
/*!40000 ALTER TABLE `reportegrupo` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportepregunta
CREATE TABLE IF NOT EXISTS `reportepregunta` (
  `idPregunta` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(255) DEFAULT NULL,
  `idCuestionario` int(11) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportepregunta: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `reportepregunta` DISABLE KEYS */;
INSERT INTO `reportepregunta` (`idPregunta`, `pregunta`, `idCuestionario`, `orden`, `idGrupo`) VALUES
	(1, 'Antes del inicio se ubicó la mesa de control, Herramientas y dispositivos coordinados en la visita.', 1, 1, 1),
	(2, 'El personal tuvo las facilidades para el Ingreso, lista de personal.', 1, 2, 1),
	(3, 'La bodega se encontró con el rotulado, bulmar, hojas de preconteo, etiquetas de manera correcta y clara antes del inicio del la lotización.', 1, 3, 1),
	(4, 'Las personas responsables de cada jerarquía se identificaron antes del inicio de la lotización.', 1, 4, 1),
	(5, 'La asistencia del personal es la establecida en la programación.', 1, 5, 1),
	(6, 'La tienda proporciono los recursos necesarios antes del inicio del conteo (cascos, escaleras u otros equipos), necesarios para realizar un optimo conteo.', 1, 6, 1),
	(7, 'Se tuvo charla de seguridad y/o prevención antes del inicio del inventario.', 1, 7, 1),
	(8, 'El inicio de lotización se realizó dentro el horario establecido en la programación.', 2, 1, 1),
	(9, 'El inicio de lotización se realizó con es responsable de cada Jerarquía de acuerdo a las pautas coordinadas', 2, 2, 1),
	(10, 'El inicio de captura se realizó dentro del horario establecido en la programación.', 2, 3, 1),
	(11, 'El inventario se realizó con la participación del personal de cada jerarquía.', 2, 4, 1),
	(12, 'Se identificaron errores en los rotulados, bulmar, hojas de preconteo, etc.', 2, 5, 1),
	(13, 'Se identificaron errores en el conteo del personal IGROUP.', 2, 6, 1),
	(14, 'Los errores con sus respectivas correcciones fueron realizadas durante el proceso y terminadas antes del termino del inventario de bodega.', 2, 7, 1),
	(15, 'El inventario termino dentro del horario establecido en la programación.', 2, 8, 1),
	(16, 'Al cierre del inventario de bodega se emitieron los reportes solicitados por el cliente.', 2, 9, 1),
	(17, 'El personal tuvo las facilidades para el ingreso, Lista de personal.', 3, 1, 1),
	(18, 'Las personas responsables de cada jerarquía se identificaron antes del inicio de lotización.', 3, 2, 1),
	(19, 'La lotización del piso de venta se realizó en el horario establecido en la programación', 3, 3, 1),
	(20, 'La asistencia del personal es la establecida en la programación.', 3, 4, 1),
	(21, 'El inicio de la lotización se realizó de acuerdo al horario establecido en la programación.', 3, 5, 1),
	(22, 'El personal tuvo las facilidades para el Ingreso, lista de personal.', 4, 1, 1),
	(23, 'La asistencia del personal es la establecida en la programación.', 4, 2, 1),
	(24, 'El inicio del inventario se realizó de acuerdo al horario establecido en la programación.', 4, 3, 1),
	(25, 'La tienda se encontró debidamente ordenada y preparada al inicio del inventario según los flujos de conteo.', 4, 4, 1),
	(26, 'El inventario se realizó con la participación de los encargados de cada jerarquía con la finalidad de atender dudas y consultas por parte de IGROUP.', 4, 5, 1),
	(27, 'La tienda proporciono los recursos necesarios para la realización del inventario (escaleras, cascos de cerveza, javas).', 4, 6, 1),
	(28, 'Se realizaron los Pre-conteos de todas las cabeceras por parte de la tienda.', 4, 7, 1),
	(29, 'Se realizaron los Pre-conteos de todas las islas o rumas por parte de la tienda', 4, 8, 1),
	(30, 'Se realizaron los Pre-conteos de todos los Laterales por parte de la tienda', 4, 9, 1),
	(31, 'Se realizaron los Pre-conteos de todas las zonas altas por parte de la tienda', 4, 10, 1),
	(32, 'Se identificaron errores por parte de la tienda en los Pre-Conteos realizados en cabeceras, laterales, rumas y zonas altas.', 4, 11, 1),
	(33, 'Los errores con sus respectivas correcciones fueron realizadas durante el proceso y terminadas antes del fin del inventario de piso venta.', 4, 12, 1),
	(34, 'El termino de la captura se realizó dentro del horario establecido en la programación.', 4, 13, 1),
	(35, 'El cierre de inventario se dio de acuerdo al horario establecido en la programación.', 4, 14, 1),
	(36, 'Al cierre del inventario se emitieron los reportes solicitados por el cliente.', 4, 15, 1);
/*!40000 ALTER TABLE `reportepregunta` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reporterespuesta
CREATE TABLE IF NOT EXISTS `reporterespuesta` (
  `idRespuesta` int(11) NOT NULL AUTO_INCREMENT,
  `respuesta` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRespuesta`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reporterespuesta: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `reporterespuesta` DISABLE KEYS */;
INSERT INTO `reporterespuesta` (`idRespuesta`, `respuesta`, `orden`, `idGrupo`) VALUES
	(1, 'SI', 1, 1),
	(2, 'NO', 2, 1);
/*!40000 ALTER TABLE `reporterespuesta` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.respuestasinforme
CREATE TABLE IF NOT EXISTS `respuestasinforme` (
  `idInforme` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `responsableIgroup` varchar(255) DEFAULT NULL,
  `responsableCliente` varchar(255) DEFAULT NULL,
  `encargadoCliente` varchar(255) DEFAULT NULL,
  `inicioConteo` varchar(50) DEFAULT NULL,
  `supervisorIgroup` varchar(255) DEFAULT NULL,
  `finConteo` varchar(50) DEFAULT NULL,
  `mesaControl` varchar(255) DEFAULT NULL,
  `cierreInventario` varchar(255) DEFAULT NULL,
  `observaciones` text,
  PRIMARY KEY (`idInforme`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.respuestasinforme: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `respuestasinforme` DISABLE KEYS */;
INSERT INTO `respuestasinforme` (`idInforme`, `fecha`, `responsableIgroup`, `responsableCliente`, `encargadoCliente`, `inicioConteo`, `supervisorIgroup`, `finConteo`, `mesaControl`, `cierreInventario`, `observaciones`) VALUES
	(1, '2018-08-10', 'CARLOS SALCEDO', 'CLIENTE ANONIMO', 'ADRIANO', 'LUNES', 'SANTIAGO', 'MARTES', NULL, NULL, 'NINGUNO EN PARTICULAR SOLO QUE GUARDE'),
	(2, NULL, NULL, NULL, '', '', '', '', '', '', '');
/*!40000 ALTER TABLE `respuestasinforme` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.respuestaspreguntas
CREATE TABLE IF NOT EXISTS `respuestaspreguntas` (
  `idPregunta` int(11) NOT NULL,
  `idRespuesta` int(11) NOT NULL,
  PRIMARY KEY (`idPregunta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.respuestaspreguntas: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `respuestaspreguntas` DISABLE KEYS */;
INSERT INTO `respuestaspreguntas` (`idPregunta`, `idRespuesta`) VALUES
	(1, 1),
	(2, 2),
	(3, 1),
	(4, 1),
	(5, 2),
	(6, 2),
	(7, 2),
	(8, 1),
	(9, 2),
	(10, 1),
	(11, 2),
	(12, 1),
	(13, 0),
	(14, 0),
	(15, 0),
	(16, 2),
	(17, 0),
	(18, 0),
	(19, 0),
	(20, 0),
	(21, 0),
	(22, 0),
	(23, 0),
	(24, 0),
	(25, 0),
	(26, 0),
	(27, 0),
	(28, 0),
	(29, 0),
	(30, 0),
	(31, 0),
	(32, 0),
	(33, 0),
	(34, 0),
	(35, 0),
	(36, 0);
/*!40000 ALTER TABLE `respuestaspreguntas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
