-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para inventariostt
CREATE DATABASE IF NOT EXISTS `inventariostt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `inventariostt`;

-- Volcando estructura para tabla inventariostt.reporte
CREATE TABLE IF NOT EXISTS `reporte` (
  `idReporte` int(11) NOT NULL AUTO_INCREMENT,
  `reporte` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idReporte`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reporte: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `reporte` DISABLE KEYS */;
INSERT INTO `reporte` (`idReporte`, `reporte`) VALUES
	(1, 'INVENTARIO - ALMACEN/BODEGA'),
	(2, 'INVENTARIO - PISO DE VENTA');
/*!40000 ALTER TABLE `reporte` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportecuestionario
CREATE TABLE IF NOT EXISTS `reportecuestionario` (
  `idCuestionario` int(11) NOT NULL AUTO_INCREMENT,
  `cuestionario` varchar(255) DEFAULT NULL,
  `idReporte` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCuestionario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportecuestionario: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `reportecuestionario` DISABLE KEYS */;
INSERT INTO `reportecuestionario` (`idCuestionario`, `cuestionario`, `idReporte`) VALUES
	(1, '1. PREPARACION DE INVENTARIO', 1),
	(2, '2. PROCESO DE CONTEO - INVENTARIO', 1),
	(3, '1. LOTIZACION - PISO VENTA', 2),
	(4, '2. CAPTURA - PISO VENTA', 2);
/*!40000 ALTER TABLE `reportecuestionario` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportecuestionariopregunta
CREATE TABLE IF NOT EXISTS `reportecuestionariopregunta` (
  `idCuestionarioPregunta` int(11) NOT NULL AUTO_INCREMENT,
  `idCuestionario` int(11) DEFAULT NULL,
  `idPregunta` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `idRespuesta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCuestionarioPregunta`),
  UNIQUE KEY `idCuestionario_idPregunta` (`idCuestionario`,`idPregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportecuestionariopregunta: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `reportecuestionariopregunta` DISABLE KEYS */;
INSERT INTO `reportecuestionariopregunta` (`idCuestionarioPregunta`, `idCuestionario`, `idPregunta`, `orden`, `idRespuesta`) VALUES
	(1, 1, 1, 1, NULL),
	(2, 1, 2, 2, NULL),
	(3, 1, 3, 3, NULL),
	(4, 1, 4, 4, NULL),
	(5, 1, 5, 5, NULL),
	(6, 1, 6, 6, NULL),
	(7, 1, 7, 7, NULL),
	(8, 2, 8, 1, NULL),
	(9, 2, 9, 2, NULL),
	(10, 2, 10, 3, NULL),
	(11, 2, 11, 4, NULL),
	(12, 2, 12, 5, NULL),
	(13, 2, 13, 6, NULL),
	(14, 2, 14, 7, NULL),
	(15, 2, 15, 8, NULL),
	(16, 2, 16, 9, NULL),
	(17, 3, 17, 1, NULL),
	(18, 3, 18, 2, NULL),
	(19, 3, 19, 3, NULL),
	(20, 3, 20, 4, NULL),
	(21, 3, 21, 5, NULL),
	(22, 4, 22, 1, NULL),
	(23, 4, 23, 2, NULL),
	(24, 4, 24, 3, NULL),
	(25, 4, 25, 4, NULL),
	(26, 4, 26, 5, NULL),
	(27, 4, 27, 6, NULL),
	(28, 4, 28, 7, NULL),
	(29, 4, 29, 8, NULL),
	(30, 4, 30, 9, NULL),
	(31, 4, 31, 10, NULL),
	(32, 4, 32, 11, NULL),
	(33, 4, 33, 12, NULL),
	(34, 4, 34, 13, NULL),
	(35, 4, 35, 14, NULL),
	(36, 4, 36, 15, NULL);
/*!40000 ALTER TABLE `reportecuestionariopregunta` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportegrupo
CREATE TABLE IF NOT EXISTS `reportegrupo` (
  `idGrupo` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idGrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportegrupo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `reportegrupo` DISABLE KEYS */;
INSERT INTO `reportegrupo` (`idGrupo`, `grupo`) VALUES
	(1, 'DICOTOMICA'),
	(2, 'INTENSION'),
	(3, 'SATISFACCION');
/*!40000 ALTER TABLE `reportegrupo` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportegruporespuesta
CREATE TABLE IF NOT EXISTS `reportegruporespuesta` (
  `idGrupoRespuesta` int(11) NOT NULL AUTO_INCREMENT,
  `idGrupo` int(11) DEFAULT NULL,
  `idRespuesta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idGrupoRespuesta`),
  UNIQUE KEY `idGrupo_idRespuesta` (`idGrupo`,`idRespuesta`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportegruporespuesta: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `reportegruporespuesta` DISABLE KEYS */;
INSERT INTO `reportegruporespuesta` (`idGrupoRespuesta`, `idGrupo`, `idRespuesta`) VALUES
	(1, 1, 1),
	(2, 1, 2);
/*!40000 ALTER TABLE `reportegruporespuesta` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportepregunta
CREATE TABLE IF NOT EXISTS `reportepregunta` (
  `idPregunta` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(255) DEFAULT NULL,
  `idGrupo` int(11) NOT NULL,
  PRIMARY KEY (`idPregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportepregunta: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `reportepregunta` DISABLE KEYS */;
INSERT INTO `reportepregunta` (`idPregunta`, `pregunta`, `idGrupo`) VALUES
	(1, 'Antes del inicio se ubicó la mesa de control, Herramientas y dispositivos coordinados en la visita.', 1),
	(2, 'El personal tuvo las facilidades para el Ingreso, lista de personal.', 1),
	(3, 'La bodega se encontró con el rotulado, bulmar, hojas de preconteo, etiquetas de manera correcta y clara antes del inicio del la lotización.', 1),
	(4, 'Las personas responsables de cada jerarquía se identificaron antes del inicio de la lotización.', 1),
	(5, 'La asistencia del personal es la establecida en la programación.', 1),
	(6, 'La tienda proporciono los recursos necesarios antes del inicio del conteo (cascos, escaleras u otros equipos), necesarios para realizar un optimo conteo.', 1),
	(7, 'Se tuvo charla de seguridad y/o prevención antes del inicio del inventario.', 1),
	(8, 'El inicio de lotización se realizó dentro el horario establecido en la programación.', 1),
	(9, 'El inicio de lotización se realizó con es responsable de cada Jerarquía de acuerdo a las pautas coordinadas', 1),
	(10, 'El inicio de captura se realizó dentro del horario establecido en la programación.', 1),
	(11, 'El inventario se realizó con la participación del personal de cada jerarquía.', 1),
	(12, 'Se identificaron errores en los rotulados, bulmar, hojas de preconteo, etc.', 1),
	(13, 'Se identificaron errores en el conteo del personal IGROUP.', 1),
	(14, 'Los errores con sus respectivas correcciones fueron realizadas durante el proceso y terminadas antes del termino del inventario de bodega.', 1),
	(15, 'El inventario termino dentro del horario establecido en la programación.', 1),
	(16, 'Al cierre del inventario de bodega se emitieron los reportes solicitados por el cliente.', 1),
	(17, 'El personal tuvo las facilidades para el ingreso, Lista de personal.', 1),
	(18, 'Las personas responsables de cada jerarquía se identificaron antes del inicio de lotización.', 1),
	(19, 'La lotización del piso de venta se realizó en el horario establecido en la programación', 1),
	(20, 'La asistencia del personal es la establecida en la programación.', 1),
	(21, 'El inicio de la lotización se realizó de acuerdo al horario establecido en la programación.', 1),
	(22, 'El personal tuvo las facilidades para el Ingreso, lista de personal.', 1),
	(23, 'La asistencia del personal es la establecida en la programación.', 1),
	(24, 'El inicio del inventario se realizó de acuerdo al horario establecido en la programación.', 1),
	(25, 'La tienda se encontró debidamente ordenada y preparada al inicio del inventario según los flujos de conteo.', 1),
	(26, 'El inventario se realizó con la participación de los encargados de cada jerarquía con la finalidad de atender dudas y consultas por parte de IGROUP.', 1),
	(27, 'La tienda proporciono los recursos necesarios para la realización del inventario (escaleras, cascos de cerveza, javas).', 1),
	(28, 'Se realizaron los Pre-conteos de todas las cabeceras por parte de la tienda.', 1),
	(29, 'Se realizaron los Pre-conteos de todas las islas o rumas por parte de la tienda', 1),
	(30, 'Se realizaron los Pre-conteos de todos los Laterales por parte de la tienda', 1),
	(31, 'Se realizaron los Pre-conteos de todas las zonas altas por parte de la tienda', 1),
	(32, 'Se identificaron errores por parte de la tienda en los Pre-Conteos realizados en cabeceras, laterales, rumas y zonas altas.', 1),
	(33, 'Los errores con sus respectivas correcciones fueron realizadas durante el proceso y terminadas antes del fin del inventario de piso venta.', 1),
	(34, 'El termino de la captura se realizó dentro del horario establecido en la programación.', 1),
	(35, 'El cierre de inventario se dio de acuerdo al horario establecido en la programación.', 1),
	(36, 'Al cierre del inventario se emitieron los reportes solicitados por el cliente.', 1);
/*!40000 ALTER TABLE `reportepregunta` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reportepreguntagrupo2222
CREATE TABLE IF NOT EXISTS `reportepreguntagrupo2222` (
  `idPreguntaGrupo` int(11) NOT NULL AUTO_INCREMENT,
  `idPregunta` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPreguntaGrupo`),
  UNIQUE KEY `idPregunta` (`idPregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reportepreguntagrupo2222: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `reportepreguntagrupo2222` DISABLE KEYS */;
INSERT INTO `reportepreguntagrupo2222` (`idPreguntaGrupo`, `idPregunta`, `idGrupo`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1),
	(6, 6, 1),
	(7, 7, 1),
	(8, 8, 1),
	(9, 9, 1),
	(10, 10, 1),
	(11, 11, 1),
	(12, 12, 1),
	(13, 13, 1),
	(14, 14, 1),
	(15, 15, 1),
	(16, 16, 1),
	(17, 17, 1),
	(18, 18, 1),
	(19, 19, 1),
	(20, 20, 1),
	(21, 21, 1),
	(22, 22, 1),
	(23, 23, 1),
	(24, 24, 1),
	(25, 25, 1),
	(26, 26, 1),
	(27, 27, 1),
	(28, 28, 1),
	(29, 29, 1),
	(30, 30, 1),
	(31, 31, 1),
	(32, 32, 1),
	(33, 33, 1),
	(34, 34, 1),
	(35, 35, 1),
	(36, 36, 1);
/*!40000 ALTER TABLE `reportepreguntagrupo2222` ENABLE KEYS */;

-- Volcando estructura para tabla inventariostt.reporterespuesta
CREATE TABLE IF NOT EXISTS `reporterespuesta` (
  `idRespuesta` int(11) NOT NULL AUTO_INCREMENT,
  `respuesta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idRespuesta`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla inventariostt.reporterespuesta: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `reporterespuesta` DISABLE KEYS */;
INSERT INTO `reporterespuesta` (`idRespuesta`, `respuesta`) VALUES
	(1, 'SI'),
	(2, 'NO'),
	(3, 'NUNCA'),
	(4, 'CASI NUNCA'),
	(5, 'A VECES'),
	(6, 'CASI SIEMPRE'),
	(7, 'SIEMPRE');
/*!40000 ALTER TABLE `reporterespuesta` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
