-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla bsgperu.grupos
CREATE TABLE IF NOT EXISTS `grupos` (
  `idGrupo` varchar(50) NOT NULL,
  `jerarquias` varchar(255) NOT NULL,
  `njerarquias` varchar(255) NOT NULL,
  `division` varchar(255) NOT NULL,
  PRIMARY KEY (`idGrupo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bsgperu.grupos: 1 rows
DELETE FROM `grupos`;
/*!40000 ALTER TABLE `grupos` DISABLE KEYS */;
INSERT INTO `grupos` (`idGrupo`, `jerarquias`, `njerarquias`, `division`) VALUES
	('Accesorios Pelo', 'Accesorios Pelo', 'Accesorios Pelo', 'Accesorios Pelo'),
	('Accesorios Textiles', 'Accesorios Textiles', 'Accesorios Textiles', 'Accesorios Textiles'),
	('Bijouterie', 'Bijouterie', 'Bijouterie', 'Bijouterie'),
	('Complementos', 'Complementos', 'Complementos', 'Complementos'),
	('Cosméticos', 'Cosméticos', 'Cosméticos', 'Cosméticos'),
	('Indumentaria', 'Indumentaria', 'Indumentaria', 'Indumentaria'),
	('Marroquinería Chica', 'Marroquinería Chica', 'Marroquinería Chica', 'Marroquinería Chica'),
	('Marroquinería Grande', 'Marroquinería Grande', 'Marroquinería Grande', 'Marroquinería Grande'),
	('Regalería', 'Regalería', 'Regalería', 'Regalería');
/*!40000 ALTER TABLE `grupos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
