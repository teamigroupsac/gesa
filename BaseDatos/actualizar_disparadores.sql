-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para disparador inventariostt.captura_before_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `captura_before_delete` BEFORE DELETE ON `captura` FOR EACH ROW BEGIN 
		INSERT INTO auditoria 
		(area_cap,barra_cap,cant_cap_ant,tip_cap,usuario,tipo,responsable)
		values 
		(OLD.area_cap,OLD.barra_cap,OLD.cant_cap,OLD.tip_cap,OLD.usuario,'ELIMINADO',OLD.responsable);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Volcando estructura para disparador inventariostt.captura_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `captura_before_update` AFTER UPDATE ON `captura` FOR EACH ROW BEGIN
	IF OLD.cant_cap <> NEW.cant_cap THEN  
		INSERT INTO auditoria 
		(area_cap,barra_cap,cant_cap_ant,cant_cap_act,tip_cap,usuario,tipo,responsable)
		values 
		(OLD.area_cap,OLD.barra_cap,OLD.cant_cap,NEW.cant_cap,OLD.tip_cap,OLD.usuario,'EDITADO',NEW.responsable);
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
