-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla inventariostt.captura_reenviado
CREATE TABLE IF NOT EXISTS `captura_reenviado` (
  `id_captura_reenviado` int(11) NOT NULL AUTO_INCREMENT,
  `area_cap` int(5) unsigned zerofill NOT NULL DEFAULT '00000',
  `barra_cap` varchar(15) NOT NULL,
  `sku_cap` varchar(13) NOT NULL DEFAULT '0',
  `cant_cap` decimal(17,3) DEFAULT NULL,
  `tip_cap` int(1) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `fecha` varchar(30) NOT NULL,
  `hora` varchar(30) NOT NULL,
  `descargado` varchar(30) NOT NULL,
  `namepalm` varchar(20) DEFAULT NULL,
  `archivo` varchar(100) DEFAULT NULL,
  `estado` int(11) DEFAULT '0',
  PRIMARY KEY (`id_captura_reenviado`),
  KEY `barra_cap` (`barra_cap`),
  KEY `area_cap` (`area_cap`)
) ENGINE=MyISAM AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
