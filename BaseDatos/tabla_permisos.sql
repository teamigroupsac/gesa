-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para inventariostt
CREATE DATABASE IF NOT EXISTS `inventariostt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `inventariostt`;

-- Volcando estructura para tabla inventariostt.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `idPerfil` int(11) NOT NULL AUTO_INCREMENT,
  `enlace` longtext NOT NULL,
  `titulo` longtext NOT NULL,
  `ventanas` longtext NOT NULL,
  `control` varchar(50) NOT NULL,
  `permiso` int(11) NOT NULL,
  PRIMARY KEY (`idPerfil`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla inventariostt.permisos: 15 rows
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`idPerfil`, `enlace`, `titulo`, `ventanas`, `control`, `permiso`) VALUES
	(1, 'usuarios', 'USUARIOS', 'usuarios', 'usuarios', 1),
	(2, 'tipo_usuario', 'TIPO USUARIO', 'tipo_usuario', 'tipo_usuario', 2),
	(3, 'tienda', 'TIENDA', 'tienda', 'tienda', 3),
	(4, 'capturas', 'CAPTURAS', 'capturas', 'capturas', 4),
	(5, 'reporte_auditoria', 'REPORTE DE AUDITORIA', 'reporte_auditoria', 'reporte_auditoria', 5),
	(6, 'busqueda_barra', 'BUSQUEDA BARRA', 'busqueda_barra', 'busqueda_barra', 6),
	(7, 'cambia_lote', 'CAMBIA LOTE', 'cambia_lote', 'cambia_lote', 7),
	(8, 'maestro', 'CARGA MAESTRO', 'maestro', 'maestro', 8),
	(9, 'stock', 'CARGA STOCK', 'stock', 'stock', 9),
	(10, 'archivos_pendientes', 'ARCHIVOS PENDIENTES', 'archivos_pendientes', 'archivos_pendientes', 10),
	(11, 'area_rango', 'AREA RANGO', 'area_rango', 'area_rango', 11),
	(12, 'generar_archivos', 'GENERAR ARCHIVOS', 'generar_archivos', 'generar_archivos', 12),
	(13, 'generar_cierre', 'REPORTES SISTEMA', 'generar_cierre', 'generar_cierre', 13),
	(14, 'cerrar_inventario', 'CERRAR INVENTARIO', 'cerrar_inventario', 'cerrar_inventario', 14),
	(15, 'gondola', 'GONDOLA', 'gondola', 'gondola', 15),
	(16, 'cierre', 'GENERAR CIERRE', 'cierre', 'cierre', 16);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
