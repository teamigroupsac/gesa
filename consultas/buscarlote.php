<?php 
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceAdministrativo.php');
   
    //LLENADO DE DATOS
    function unicode_decode($str) {
        return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $str);
    }

    $service = new ServiceAdministrativo();

    $lote = $_POST["lote"];

    $data = $service->buscarLote($lote);

    $resultado = Array();
    foreach ($data as $fila) {
        $registro = new stdClass();
        $registro->barra_cap = $fila->barra_cap;
        $registro->descripcion = unicode_decode($fila->descripcion);
        $registro->cant_cap = $fila->cant_cap;
        
        $resultado[] = $registro;
    }
    if(count($data) > 0){
        header('Content-Type: application/json');
        header('X-PHP-Response-Code: 200', true);

        //$array['resultado'] = array_values($resultado);
        echo json_encode($resultado, JSON_PRETTY_PRINT);
        exit();
    }else{
        echo 0;
    }
    
    //header('Content-Type: application/json');
    //header('X-PHP-Response-Code: 200', true);

    //$array['resultado'] = array_values($resultado);
    //echo json_encode($array, JSON_PRETTY_PRINT);
    //exit();

?>
